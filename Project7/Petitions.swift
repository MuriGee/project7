//
//  Petitions.swift
//  Project7
//
//  Created by Muri Gumbodete on 22/03/2022.
//

import Foundation

struct Petitions: Codable {
    var results: [Petition]
}
